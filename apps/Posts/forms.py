from django import forms
from .models import Post

class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['mail','title','text']

        labels = {
            'mail': 'Correo Eléctronico',
            'title': 'Título',
            'text': 'Mensaje',
            
        }
        widgets = {
            'mail': forms.EmailInput(attrs={'class': 'form-control'}),
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'text': forms.Textarea(attrs={'class': 'form-control'}),
        }
        
