from django.urls import path, include
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from apps.Posts import views

urlpatterns = [   
    # localhost:8000
    path('listar_posts', views.PostList.as_view(), name='listar_posts'),
    # localhost:8000/post/new
    path('new/', views.PostCreate.as_view(), name='contacto'),

    path('api/', views.API_objects.as_view()),

    path('api/<int:pk>/', views.API_objects_details.as_view()),
    
]


urlpatterns = format_suffix_patterns(urlpatterns)