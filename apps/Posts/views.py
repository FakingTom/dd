from django.shortcuts import render
from django.utils import timezone
from .models import Post
from django.shortcuts import render, get_object_or_404
from .forms import PostForm
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.contrib.auth.models import User
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from rest_framework import generics
from .serializers import PostSerializer


class PostCreate(CreateView):
    model = Post
    form_class = PostForm
    
    template_name = 'Posts/contacto.html'
    success_url = reverse_lazy("contacto")
    

class PostList(ListView):
    model = Post
    template_name = 'Posts/post_list_ext.html'
    # paginate_by = 4

#---Clases para API---
class API_objects(generics.ListCreateAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class API_objects_details(generics.RetrieveUpdateDestroyAPIView):
    queryset = Post.objects.all()
    serializer_class = PostSerializer