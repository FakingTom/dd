from django.db import models
from django.utils import timezone


class Post(models.Model):
        mail = models.CharField(max_length=70, null=False)
        title = models.CharField(max_length=50)
        text = models.TextField()
        created_date = models.DateTimeField(default=timezone.now)


def __str__(self):
        return self.title
