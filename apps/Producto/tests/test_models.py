from django.test import TestCase
from django.template.defaultfilters import slugify
from apps.Producto.models import Producto

class TestCase(TestCase):
    def setUp(self):
        Producto.objects.create(nombre="Hallullas",descripcion="Kilo de hallulla clásica de Dulce Despensa", precio=1100)
        Producto.objects.create(nombre="Marraquetas",descripcion="Kilo de marraqueta clásica de Dulce Despensa ", precio=1100)

    def test_ingresar_Productos(self):

        "Los productos se registran correctamente en la BD"

        Producto_1 = Producto.objects.get(nombre="Hallullas")
        Producto_2 = Producto.objects.get(nombre="Marraquetas")
        self.assertEqual(Producto_1.precio, 1100)
        self.assertEqual(Producto_2.precio, 1100)