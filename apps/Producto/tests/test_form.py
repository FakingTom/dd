from django.test import TestCase
from apps.Producto.models import Producto
from apps.Producto.forms import ProductoForm

class ProductoFormCase(TestCase):
        
    def test_invalid_form(self):

        "Formulario incompleto es incorrecto"

        producto = Producto.objects.create(nombre='', descripcion="Kilo de hallulla clásica de Dulce Despensa", precio=1100)
        data = {'nombre': Producto.nombre, 'descripcion': Producto.descripcion,'precio': Producto.precio, }
        form = ProductoForm(data=data)
        self.assertFalse(form.is_valid())