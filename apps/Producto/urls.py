from django.urls import path, include
from . import views
from rest_framework.urlpatterns import format_suffix_patterns
from apps.Producto import views

urlpatterns = [

    #Productos
    path('catalogo/', views.ProductoList.as_view(), name='catalogo'),

    path('listar_producto/', views.ProductoList2.as_view(), name='listar_producto'),

    path('add_producto/', views.ProductoCreate.as_view(), name='add_producto'),

    path('edit_producto/<int:pk>', views.ProductoUpdate.as_view(), name='edit_producto'),

    path('del_producto/<int:pk>', views.ProductoDelete.as_view(), name='del_producto'),

    path('<int:pk>', views.Producto, name='Productos'),
    #Url API
    path('api/', views.API_objects.as_view()),

    path('api/<int:pk>/', views.API_objects_details.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
